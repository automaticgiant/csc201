/**
 * Project 7 Triangle class
 */

/**
 * The Triangle class extends GeometricObject to give it more triangle powers.
 * 
 * @author automaticgiant
 * @version 10/20/12
 */
public class Triangle extends GeometricObject {
	/**
	 * a side of the triangle
	 */
	@SuppressWarnings("javadoc")
	// it was warning about side2,3 but the javadoc worked fine for all three
	private double	side1	= 1, side2 = 1, side3 = 1;

	/**
	 * The default constructor makes a Triangle with the superclass's
	 * constructor and adds in the triangle powers such as default side lengths.
	 */
	public Triangle() {
		super();
	}

	/**
	 * Generates a new Triangle object and sets the side lengths.
	 * 
	 * @param side1
	 *            a side of the triangle
	 * @param side2
	 *            a side of the triangle
	 * @param side3
	 *            a side of the triangle
	 */
	public Triangle(double side1, double side2, double side3) {
		setSide1(side1);
		setSide2(side2);
		setSide3(side3);
	}

	/**
	 * Accessor for triangle's area. No stale data here!
	 * 
	 * @return String containing triangle area
	 */
	public String getArea() {
		// semiperimeter - used 4x so probably saves a few cycles
		double s = Double.valueOf(getPerimeter()).doubleValue() / 2;
		// Heron's formula
		double area = Math.sqrt(s * (s - getSide1()) * (s - getSide2())
				* (s - getSide3()));
		return Double.toString(area);
	}

	/**
	 * perimeter accessor
	 * 
	 * @return string containing perimeter
	 */
	public String getPerimeter() {
		return Double.toString(getSide1() + getSide2() + getSide3());
	}

	/**
	 * side 1 accessor
	 * 
	 * @return the side1
	 */
	public double getSide1() {
		return this.side1;
	}

	/**
	 * side 2 accessor
	 * 
	 * @return the side2
	 */
	public double getSide2() {
		return this.side2;
	}

	/**
	 * side 3 accessor
	 * 
	 * @return the side3
	 */
	public double getSide3() {
		return this.side3;
	}

	/**
	 * side 1 mutator
	 * 
	 * @param side1
	 *            the side1 to set
	 */
	public void setSide1(double side1) {
		this.side1 = side1;
	}

	/**
	 * side 2 mutator
	 * 
	 * @param side2
	 *            the side2 to set
	 */
	public void setSide2(double side2) {
		this.side2 = side2;
	}

	/**
	 * side 3 mutator
	 * 
	 * @param side3
	 *            the side3 to set
	 */
	public void setSide3(double side3) {
		this.side3 = side3;
	}

	/*
	 * (non-Javadoc)
	 * @see GeometricObject#toString()
	 */
	@SuppressWarnings("javadoc")
	@Override
	public String toString() {
		return "Triangle Information:\nside1 = " + this.side1 + "\nside2 = "
				+ this.side2 + "\nside3 = " + this.side3 + "\ncolor = "
				+ getColor() + "\nfilled  = " + String.valueOf(isFilled());
	}
}