// only 2 warnings here. javadoc warnings (not documented)
public class DemoTriangle {
	public static void main(String[] args) {
		Triangle triangle = new Triangle(1, 1.5, 1);
		System.out.println(triangle);

		triangle.setColor("yellow");
		triangle.setFilled(true);

		System.out.println("The area is " + triangle.getArea());
		System.out.println("The perimeter is " + triangle.getPerimeter());
		System.out.println(triangle);
	}
}