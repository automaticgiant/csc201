import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * DemoClass demos the Month class.
 * <p>
 * 10/11/12
 * @author Hunter Morgan
 */
class DemoClass {

	/**
	 * Month: pseudo-inner month name/number class.
	 * <p>
	 * in that it is inner in the same source but static gets it compiled into its own class
	 */
	public static class Month {
		// vars:
		/**
		 * this is the one -actual- field of the Month class - properly encapsulated in appropriate methods
		 */
		private int monthNumber;

		/**
		 * this is one side of the bidirectional map used to convert name<->number
		 */
		private static BiMap<String, Integer> monthNameToNumber = HashBiMap.create();
		{ // load it up
			monthNameToNumber.put("January",1);
			monthNameToNumber.put("February",2);
			monthNameToNumber.put("March",3);
			monthNameToNumber.put("April",4);
			monthNameToNumber.put("May",5);
			monthNameToNumber.put("June",6);
			monthNameToNumber.put("July",7);
			monthNameToNumber.put("August",8);
			monthNameToNumber.put("September",9);
			monthNameToNumber.put("October",10);
			monthNameToNumber.put("November",11);
			monthNameToNumber.put("December",12);
		}
		/**
		 * inverse bimap view (one structure w/ 2 views)
		 */
		private static BiMap<Integer, String> monthNumberToName = monthNameToNumber.inverse(); //invert it

		
		//constructors:
		/**
		 * no-arg constructor - sets monthNumber to 1
		 */
		public Month() {
			this.setMonthNumber(1);
		}
		
		/**
		 * constructor that accepts number of month as argument - invalid number defaults to 1
		 */
		public Month(int number) {
			this.setMonthNumber(number);
		}
		
		/**
		 * constructor that accepts name as argument and sets number accordingly
		 * <p>
		 * Capitalization is essential - the bimap is case sensitive.
		 */
		public Month(String name) {
			//the best way to handle this inside the class would be to throw an exception i think
			//but i haven't gotten into exception handling yet
			if (monthNameToNumber.get(name) == null) System.out.println(
					"Proper Capitalization is Key! This call didn't take.");
			else this.setMonthNumber(monthNameToNumber.get(name));
			
		}

		//accessors:
		/**
		 * monthNumber accessor
		 * @return monthNumber
		 */
		public int getMonthNumber() {
			return monthNumber;
		}
		
		/**
		 * monthName accessor
		 * @return monthName
		 */
		public String getMonthName() {
			return monthNumberToName.get(this.getMonthNumber());
		}
		
		/**
		 * standard toString method specification
		 * @return monthName
		 */
		public String toString() {
			return this.getMonthName();
		}
		
		/**
		 * standard equals method specification
		 * @return truth - not a variable, just literal boolean values
		 */
		public boolean equals(Month targetMonth) {
			if (this.getMonthNumber() == targetMonth.getMonthNumber()) return true;
			else return false;
		}
		
		/**
		 * standard greater than method specification
		 * @return truth - not a variable, just literal boolean values
		 */
		public boolean greaterThan(Month targetMonth) {
			if (this.getMonthNumber() > targetMonth.getMonthNumber()) return true;
			else return false;
		}

		/**
		 * standard less than method specification
		 * @return truth - not a variable, just literal boolean values
		 */
		public boolean lessThan(Month targetMonth) {
			if (this.getMonthNumber() < targetMonth.getMonthNumber()) return true;
			else return false;
		}
		
		//mutators:
		/**
		 * monthNumber mutator
		 * @param number the monthNumber to set
		 */
		public void setMonthNumber(int number) {
			if (number < 1 || number > 12) this.setMonthNumber(number);
			else this.monthNumber = number;
		}
		
	}

	
	/**
	 * The main method of DemoClass demos the Month class.
	 * <p>
	 * It utilizes all constructors, accessors,
	 * the sole mutator, and arrays, for good measure. It uses some simple if/else tests to validate
	 * and demonstrate.
	 */
	public static void main(String[] args) {
		//declare requisite months
		Month month1, month2, month3;
		//group in an array and instantiate
		Month quarter[] = {month1 = new Month(), month2 = new Month(2), month3 = new Month("March")};
		
		//demonstrate arrays, getMonthName, toString, lessThan
		System.out.println("quarter[0], " + quarter[0].getMonthName() + " is less than month2, " + 
				month2.toString() + "?");
		if (quarter[0].lessThan(month2)) System.out.println("true!");
		else System.out.println("false!");
		
		//demonstrate greaterThan
		System.out.println("quarter[2], " + quarter[2].getMonthName() + " is greater than month3?");
		if (quarter[1].greaterThan(month3)) System.out.println("true!");
		else System.out.println("false!");
		
		//demonstrate equals twice and name constructor again
		System.out.println("Does quarter[1] = month1 = January?");
		if (quarter[0].equals(month1) && month1.equals(new Month("January"))) System.out.println("true!");
		else System.out.println("false!");		
	}

}
