/**
 * gui class file
 */
package skateShop;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 * gui class
 * 
 * @author Hunter
 */
@SuppressWarnings("serial")
public class BoardBuilderGUI extends JFrame implements ActionListener,
		ListSelectionListener {
	// data
	/**
	 * selectedDeckPrice
	 */
	double selectedDeckPrice;
	/**
	 * selectedTruckPrice
	 */
	double selectedTruckPrice;
	/**
	 * selectedWheelsPrice
	 */
	double selectedWheelsPrice;
	/**
	 * gripTapePrice
	 */
	final double gripTapePrice = 10;
	/**
	 * bearingsPrice
	 */
	final double bearingsPrice = 30;
	/**
	 * riserPadsPrice
	 */
	final double riserPadsPrice = 2;
	/**
	 * nutsAndBoltsKitPrice
	 */
	final double nutsAndBoltsKitPrice = 3;
	/**
	 * this formats doubles into dollar amounts
	 */
	NumberFormat currencyFormatter =
			NumberFormat.getCurrencyInstance(getLocale());
	
	// gui elements
	/**
	 * this is the main container for elements of a BoardBuilderGUI object
	 */
	JPanel main = new JPanel(new GridLayout(0, 3),
			true);
	/**
	 * selection list for decks
	 */
	// formatter:off
	JList<String> decks = new JList<>(new String[] {
			"The Master Thrasher $60", "The Dictator $45",
			"The Street King $50"
	});
	/**
	 * selection list for trucks
	 */
	JList<String> trucks = new JList<>(new String[] {
			"7.75- inch axle $35", "8- inch axle $40", "8.5- inch axle $45"
	});
	/**
	 * selection list for wheel sets
	 */
	JList<String> wheels = new JList<>(new String[] {
			"51 mm $20", "55 mm $22", "58 mm $24", "61 mm $28"
	});
	// formatter:on
	// /**
	// * panel to hold extras labels
	// */
	// JPanel extras1 = new JPanel(new GridLayout(0, 1));
	/**
	 * panel to hold extras input boxes
	 */
	JPanel extras = new JPanel(new GridLayout(0, 1));
	/**
	 * a field to input grip Tape qty
	 */
	JTextField gripTapeField = new JTextField("0");
	/**
	 * a field to input bearings qty
	 */
	JTextField bearingsField = new JTextField("0");
	/**
	 * a field to riser pad qty
	 */
	JTextField riserPadsField = new JTextField("0");
	/**
	 * a field to input nuts and bolts kit qty
	 */
	JTextField nutsAndBoltsKitField = new JTextField("0");
	/**
	 * the label that will display the output figures
	 */
	JLabel figures = new JLabel();
	
	/**
	 * generate figures for output
	 * 
	 * @return string of formatted output
	 */
	@SuppressWarnings("boxing")
	String getFigures() {
		Double subtotal = Double.valueOf(getListSelectedPrice(this.decks))
				+ Double.valueOf(getListSelectedPrice(this.trucks))
				+ Double.valueOf(getListSelectedPrice(this.wheels))
				+ Double.valueOf(this.gripTapeField.getText())
				+ Double.valueOf(this.bearingsField.getText())
				+ Double.valueOf(this.riserPadsField.getText())
				+ Double.valueOf(this.nutsAndBoltsKitField.getText());
		return "<html>Subtotal: " + this.currencyFormatter.format(subtotal)
				+ "<br>Tax: "
				+ this.currencyFormatter.format(.6 * subtotal).toString()
				+ "<br>Total: "
				+ this.currencyFormatter.format(1.6 * subtotal).toString()
				+ "</html>";
	}
	
	/**
	 * this grabs a list selection and parses it for a price
	 * 
	 * @param targetList
	 *            this is the list we want to grab from
	 * @return the price of the item
	 */
	@SuppressWarnings("boxing")
	static double getListSelectedPrice(JList<String> targetList) {
		return Double.valueOf(targetList.getSelectedValue().toString()
				.replaceFirst(".*\\$", ""));
	}
	
	/**
	 * default constructor that assembles the gui
	 */
	public BoardBuilderGUI() {
		this.main.add(new JLabel("Deck Choices:"));
		this.main.add(new JLabel("Truck Choices:"));
		this.main.add(new JLabel("Wheel Choices:"));
		this.main.add(this.decks);
		this.decks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.decks.setSelectionInterval(0, 0);
		this.decks.addListSelectionListener(this);
		this.main.add(this.trucks);
		this.trucks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.trucks.setSelectionInterval(0, 0);
		this.trucks.addListSelectionListener(this);
		this.main.add(this.wheels);
		this.wheels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		this.wheels.setSelectionInterval(0, 0);
		this.wheels.addListSelectionListener(this);
		// this.main.add(this.extras1);
		this.main
				.add(new JLabel(
						"<html>Grip Tape $10<br>Bearings $30<br>Riser Pads $2<br>Nuts and Bolts Kit $3</html>"));
		this.main.add(this.extras);
		this.extras.add(this.gripTapeField);
		this.gripTapeField.addActionListener(this);
		// this.extras1.add(new JLabel("Bearings $30"));
		this.extras.add(this.bearingsField);
		this.bearingsField.addActionListener(this);
		// this.extras1.add(new JLabel("Riser Pads $2"));
		this.extras.add(this.riserPadsField);
		this.riserPadsField.addActionListener(this);
		// this.extras1.add(new JLabel("Nuts and Bolts Kit $3"));
		this.extras.add(this.nutsAndBoltsKitField);
		this.nutsAndBoltsKitField.addActionListener(this);
		// this.main.add(new JLabel());
		this.main.add(this.figures);
		this.add(this.main);
		refresh();
		// this.setVisible(true);
		
	}
	
	@Override
	public void valueChanged(ListSelectionEvent arg0) {
		refresh();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		refresh();
	}
	
	/**
	 * this redisplays the only dynamic output of the application
	 */
	private void refresh() {
		this.figures.setText(getFigures());
	}
	
	// Interesting. Eclipse suggests these when creating a class that inherits
	// from JFrame.
	// /**
	// * @throws HeadlessException
	// */
	// public BoardBuilderGUI() throws HeadlessException {
	// // TODO Auto-generated constructor stub
	// }
	//
	// /**
	// * @param arg0
	// */
	// public BoardBuilderGUI(GraphicsConfiguration arg0) {
	// super(arg0);
	// // TODO Auto-generated constructor stub
	// }
	//
	// /**
	// * @param arg0
	// * @throws HeadlessException
	// */
	// public BoardBuilderGUI(String arg0) throws HeadlessException {
	// super(arg0);
	// // TODO Auto-generated constructor stub
	// }
	//
	// /**
	// * @param arg0
	// * @param arg1
	// */
	// public BoardBuilderGUI(String arg0, GraphicsConfiguration arg1) {
	// super(arg0, arg1);
	// // TODO Auto-generated constructor stub
	// }
	
}
