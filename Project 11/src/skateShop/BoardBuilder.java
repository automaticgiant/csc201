/**
 * this is the class file for the gui driver
 */
package skateShop;

import javax.swing.JFrame;

/**
 * @author Hunter
 */
class BoardBuilder {
	
	/**
	 * Driver for the BoardBuilderGUI
	 * 
	 * @param args
	 *            moot
	 */
	public static void main(String[] args) {
		JFrame window = new BoardBuilderGUI();
		final int WINDOW_WIDTH = 900, WINDOW_HEIGHT = 250;
		window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocationRelativeTo(null);
		window.setVisible(true);
	}
	
}
