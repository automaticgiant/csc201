// Car class
public class Car {
	// declarations for attributes
	private int yearModel;
	private String make;
	private int speed;
	// and its constructor
	public Car(int year, String manuf) {
		this.yearModel = year;
		this.make = manuf;
		this.speed = 0;
	}

	// Methods:
	
	// yearModel accessor
	/**
	 * @return the yearModel
	 */
	public int getYearModel() {
		return yearModel;
	}
	
	// yearModel mutator
	/**
	 * @param year, the yearModel to set
	 */
	public void setYearModel(int year) {
		this.yearModel = year;
	}
	
	// make accessor
	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}
	
	// make mutator
	/**
	 * @param manuf, the make to set
	 */
	public void setMake(String manuf) {
		this.make = manuf;
	}
	
	// speed accessor
	/**
	 * @return the speed
	 */
	public int getSpeed() {
		return speed;
	}
	
	
	// speed mutator
	/**
	 * @param spd, the speed to set
	 */
	public void setSpeed(int spd) {
		this.speed = spd;
	}
	
	// How we will really be changing the speed atribute:	
	public void accelerate() {
		this.speed += 5;
	}
	public void brake() {
		this.speed -= 5;
	}
	
	// Pretty report generator
	public String status() {
		String output = "The " + getYearModel() + " " + getMake() + " is now travelling at a speed of " + getSpeed() + ".";
		return output;
	}
	
}

