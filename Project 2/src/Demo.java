// Car class demo
public class Demo {
	public static void main(String[] args) {
		// instantiate a car
		Car car0 = new Car(1971,"Ford");
		// pretty self explanitory - speed up and slow down while displaying speeds
		car0.accelerate();
		System.out.println(car0.status());
		car0.accelerate();
		System.out.println(car0.status());
		car0.accelerate();
		System.out.println(car0.status());
		car0.accelerate();
		System.out.println(car0.status());
		car0.accelerate();
		System.out.println(car0.status());
		car0.brake();
		System.out.println(car0.status());
		car0.brake();
		System.out.println(car0.status());
		car0.brake();
		System.out.println(car0.status());
		car0.brake();
		System.out.println(car0.status());
		car0.brake();
		System.out.println(car0.status());
		
		// if you like System.exit(), you'll like this, though, like the last program,
		// it is not strictly necessary, as it is single threaded and exits cleanly.
		System.exit(0);
		
	}

}
