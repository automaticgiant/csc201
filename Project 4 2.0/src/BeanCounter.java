import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * BeanCounter class: reads integers, finds the largest of them, and counts its occurrences.
 * <p>
 * 10/4/2012
 * @author Hunter Morgan
 * @version 2.0 - see emailed jar
 * */
class BeanCounter {

	/**
	 * No args, just procedure.
	 * No reason not to have everything in the main function.
	 */
	public static void main(String[] args) {
		// preseed a non loop-killing input/control variable
		int num = 1;
		// preseed a logical max-count variable 
		int max = 0;
		// set up for keyboard input through Scanner
		Scanner kbIn = new Scanner(System.in);
		/* here is where the magic happens. hashes are groovy. it efficiently provides pidgeon-
		 * holes for all the input ints without a huge mess. admittedly less useful when only
		 * one needs to be used at the end, but i was already here dammit!
		 */
		HashMap<Integer, Integer> counts = new HashMap<Integer, Integer>();
		// prompt
		
		System.out.print("Enter numbers: ");
		// input loop with provision for sentinal, 0
		
		while ( num != 0 ) {
			num = kbIn.nextInt();
			// add 1 to existing hole or create a new one
			if (counts.containsKey(num) == false) counts.put(num, 1);
			else counts.put(num, counts.get(num) + 1);
		}

		// iterate through key set to establish max
		Set<Integer> testingSet = counts.keySet();
		for ( int key : testingSet ) if ( key > max ) max = key;
		
		// report findings
		System.out.println("The largest number is " + max);
		System.out.print("The occurrence count of the largest number is " + counts.get(max));

		// would have cleaned up by turning references null and closing kbIn, but show's over anyway
		
	}
}
