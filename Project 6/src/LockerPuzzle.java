/**
 * LockerPuzzle runs a full simulation of the premise to solve the puzzle.
 * <p>
 * Watch this in a minimum 100 char wide terminal.
 * <p>
 * Note: I just turned warnings way up in eclipse, and this one is now clean,
 * but I have 124 items on all the other projects I have loaded. LOL.
 * 
 * @author Hunter Morgan
 */
class LockerPuzzle {

	/**
	 * main initializes a few things and runs the sim with some for loops
	 * 
	 * @param args
	 *            don't worry about this - ignored
	 */
	public static void main(String[] args) {
		// the prescribed boolean array representing locker door status
		// i decided i could waste one slot for more transparent code
		// the only reason java uses 1-indexed arrays is tradition
		boolean[] lockers = new boolean[101];
		// they all start closed.
		// only need fill once, why not just use an explicit call.
		java.util.Arrays.fill(lockers, false);
		// this loop sims each of the 100 students
		for (int student = 1; !(student > 100); student++) {
			/*
			 * this loop sims the student's interaction with the proper lockers.
			 * we increment locker by student as that is the specified behavior.
			 */
			for (int locker = student; !(locker > 100); locker += student) {
				// flip the locker state
				lockers[locker] = !lockers[locker];
				// status display
				for (int i = 1; !(i > 100); i++) {
					// show open/closed
					if (i != locker) {
						if (lockers[i]) System.out.print("1");
						else System.out.print("0");
					}
					else
					// show a change differently
					if (lockers[i]) System.out.print("+");
					else System.out.print("-");
				}
				// throw a newline to make it look nice
				System.out.println();
				// delay for better visualization
				try {
					Thread.sleep(75);
				} catch (InterruptedException e) {
					// We're not actually going to try to catch this.
				}
			}
		}
		System.out.print("Lockers");
		for (int i = 1; !(i > 100); i++) {
			// show open
			if (lockers[i]) System.out.print(" " + i);
		}
		System.out.println(" open.");
	}
}