/**
 * Here, the Payroll Exceptions are defined.
 */
package project9;

/**
 * PayrollException Superclass
 * 
 * @author automaticgiant
 */
class PayrollException extends Exception {

	/**
	 * @param message
	 *            reason for exception
	 */
	public PayrollException(String message) {
		super(message);
	}

}

/**
 * PayrollException for when an empty string is given for the employee’s name.
 * 
 * @author automaticgiant
 */
class PayrollNameException extends PayrollException {

	/**
	 * @param message
	 *            reason for exception
	 */
	public PayrollNameException(String message) {
		super(message);
	}
}

/**
 * PayrollException for when an invalid value is given for the employee’s ID
 * number. A negative number or zero would be invalid.
 * 
 * @author automaticgiant
 */
class PayrollIdException extends PayrollException {

	/**
	 * @param message
	 *            reason for exception
	 */
	public PayrollIdException(String message) {
		super(message);
	}
}

/**
 * PayrollException for when an invalid number is given for the number of hours
 * worked. This would be a negative number or a number greater than 84.
 * 
 * @author automaticgiant
 */
class PayrollHoursException extends PayrollException {

	/**
	 * @param message
	 *            reason for exception
	 */
	public PayrollHoursException(String message) {
		super(message);
	}
}

/**
 * PayrollException for when an invalid number is given for the hourly pay rate.
 * This would be a negative number or a number greater than 25.
 * 
 * @author automaticgiant
 */
class PayrollRateException extends PayrollException {

	/**
	 * @param message
	 *            reason for exception
	 */
	public PayrollRateException(String message) {
		super(message);
	}
}