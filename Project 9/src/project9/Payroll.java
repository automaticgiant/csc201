package project9;

/**
 * Payroll Class The Payroll class stores payroll data.
 */

public class Payroll {
	/**
	 * Employee name
	 */
	private String	name;
	/**
	 * ID number
	 */
	private int		idNumber;
	/**
	 * Hourly pay rate
	 */
	private double	payRate;
	/**
	 * Number of hours worked
	 */
	private double	hoursWorked;

	/**
	 * The constructor initializes an object with the employee's name and ID
	 * number.
	 * 
	 * @param n
	 *            name
	 * @param i
	 *            id
	 * @throws PayrollIdException
	 *             if id is bad
	 * @throws PayrollNameException
	 *             if name is bad
	 */

	public Payroll(String n, int i) throws PayrollIdException,
			PayrollNameException {
		setName(n);
		setIdNumber(i);
	}

	/**
	 * The setName sets the employee's name.
	 * 
	 * @param n
	 *            name to set
	 * @throws PayrollNameException
	 *             if name is bad
	 */

	public void setName(String n) throws PayrollNameException {
		if (n.equals(""))
			throw new PayrollNameException("Null name unacceptable");
		this.name = n;
	}

	/**
	 * The setIdNumber sets the employee's ID number.
	 * 
	 * @param i
	 *            id to set
	 * @throws PayrollIdException
	 *             if id is bad
	 */

	public void setIdNumber(int i) throws PayrollIdException {
		if (i < 0)
			throw new PayrollIdException("Negative id number unacceptable");
		if (i == 0) throw new PayrollIdException("Id number 0 unacceptable");
		this.idNumber = i;
	}

	/**
	 * The setPayRate sets the employee's pay rate.
	 * 
	 * @param p
	 *            pay rate
	 * @throws PayrollRateException
	 *             if pay rate is fubar
	 */

	public void setPayRate(double p) throws PayrollRateException {
		if (p > 25) throw new PayrollRateException("we don't pay that much!");
		if (p < 0)
			throw new PayrollRateException(
					"We dont get paid to employ people (yet)");
		this.payRate = p;
	}

	/**
	 * The setHoursWorked sets the number of hours worked.
	 * 
	 * @param h
	 *            hours worked
	 * @throws PayrollHoursException
	 *             if hours don't make sense
	 */

	public void setHoursWorked(double h) throws PayrollHoursException {
		if (h < 0)
			throw new PayrollHoursException("that doesn't make any sense!");
		if (h == 0)
			throw new PayrollHoursException("we don't have a base pay");
		if (h > 84) throw new PayrollHoursException("thats too many hours!");
		this.hoursWorked = h;
	}

	/**
	 * The getName returns the employee's name.
	 * 
	 * @return name of employee
	 */

	public String getName() {
		return this.name;
	}

	/**
	 * The getIdNumber returns the employee's ID number.
	 * 
	 * @return id number of worker
	 */

	public int getIdNumber() {
		return this.idNumber;
	}

	/**
	 * The getPayRate returns the employee's pay rate.
	 * 
	 * @return payrate of worker
	 */

	public double getPayRate() {
		return this.payRate;
	}

	/**
	 * The getHoursWorked returns the hours worked by the employee.
	 * 
	 * @return hours worked by worker
	 */

	public double getHoursWorked() {
		return this.hoursWorked;
	}

	/**
	 * The getGrossPay returns the employee's gross pay.
	 * 
	 * @return gross pay for worker
	 */

	public double getGrossPay() {
		return this.hoursWorked * this.payRate;
	}
}