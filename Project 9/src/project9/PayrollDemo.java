package project9;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Payroll Class This program demonstrates the Payroll class.
 */

public class PayrollDemo {
	/**
	 * this is a graceful exit method that instead of stack tracing, explains
	 * why it has to die
	 * 
	 * @param error
	 *            the exception to explain
	 */
	public static void gracefulExit(Exception error) {
		System.out.println("Impossible! Reason(non-exhaustive):");
		System.out.println(error.toString());
		System.exit(1);
	}

	/**
	 * where the magic happens. demo's the payroll class
	 * 
	 * @param args
	 *            unused args
	 */
	public static void main(String[] args) {
		// Variables for input
		String name; // An employee's name
		int id = 0; // An employee's ID number
		double payRate = 0; // An employee's pay rate
		double hoursWorked = 0; // The number of hours worked

		// Create a Scanner object for keyboard input.
		Scanner keyboard = new Scanner(System.in);

		// Get the employee's name.
		System.out.print("Enter the employee's name: ");
		name = keyboard.nextLine();

		try {
			// Get the employee's ID number.
			System.out.print("Enter the employee's ID number: ");
			id = keyboard.nextInt();
		} catch (InputMismatchException e) {
			gracefulExit(e);
		}

		try {
			// Get the employee's pay rate.
			System.out.print("Enter the employee's hourly pay rate: ");
			payRate = keyboard.nextDouble();
		} catch (InputMismatchException e) {
			gracefulExit(e);
		}

		try {
			// Get the number of hours worked by the employee.
			System.out.print("Enter the number of hours worked "
					+ " by the employee: ");
			hoursWorked = keyboard.nextDouble();
		} catch (InputMismatchException e) {
			gracefulExit(e);
		}

		// Create a Payroll object and store the data in it.
		Payroll worker = null;
		try {
			worker = new Payroll(name, id);
		} catch (PayrollIdException e) {
			gracefulExit(e);
		} catch (PayrollNameException e) {
			gracefulExit(e);
		}
		try {
			worker.setPayRate(payRate);
		} catch (PayrollRateException e) {
			gracefulExit(e);
		}
		try {
			worker.setHoursWorked(hoursWorked);
		} catch (PayrollHoursException e) {
			gracefulExit(e);
		}

		// Display the employee's payroll data.
		System.out.println("\nEmployee Payroll Data");
		System.out.println("Name: " + worker.getName());
		System.out.println("ID Number: " + worker.getIdNumber());
		System.out.println("Hourly pay rate: " + worker.getPayRate());
		System.out.println("Hours worked: " + worker.getHoursWorked());
		System.out.println("Gross pay: $" + worker.getGrossPay());
	}
}