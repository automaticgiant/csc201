/**
 * Project 12 - Soda Machine Applet
 * @author Hunter Morgan
 */
package vending;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * main soda machine class
 */
public class SodaMachine extends JApplet implements ActionListener {
	/**
	 * inventory in machine for beverage
	 */
	@SuppressWarnings("javadoc")
	int limeVentory = 20, grapeVentory = 20, rootbeerVentory = 20,
			waterVentory = 20, colaVentory = 20;
	/**
	 * price point for beverages
	 */
	double cost = .75;
	/**
	 * pre-populate money slot with a dollar
	 */
	double moneyIn = 1;
	/**
	 * formatter for money amounts
	 */
	NumberFormat currencyFormatter = NumberFormat
			.getCurrencyInstance(getLocale());
	/**
	 * way to access applet resources
	 */
	ClassLoader cldr = this.getClass().getClassLoader();
	/**
	 * lightbulb on/stocked icon
	 */
	ImageIcon in = new ImageIcon(this.cldr.getResource("vending/on.jpg"));
	/**
	 * lightbulb out/out of stock icon
	 */
	ImageIcon out = new ImageIcon(this.cldr.getResource("vending/off.jpg"));
	/**
	 * status light state for lime
	 */
	ImageIcon limeStatus = this.in;
	/**
	 * status light state for grape
	 */
	ImageIcon grapeStatus = this.in;
	/**
	 * status light state for root beer
	 */
	ImageIcon rootbeerStatus = this.in;
	/**
	 * status light state for water
	 */
	ImageIcon waterStatus = this.in;
	/**
	 * status light state for cola
	 */
	ImageIcon colaStatus = this.in;
	/**
	 * image for non-specific beverage
	 */
	ImageIcon can = new ImageIcon(this.cldr.getResource("vending/can.jpg"));
	/**
	 * take money here
	 */
	JTextField input = new JTextField(String.valueOf(this.moneyIn));
	/**
	 * button to buy lime
	 */
	JButton limeButton = new JButton(new ImageIcon(
			this.cldr.getResource("vending/lime.jpg")));
	/**
	 * button to buy grape
	 */
	JButton grapeButton = new JButton(new ImageIcon(
			this.cldr.getResource("vending/grape.jpg")));
	/**
	 * button to buy root beer
	 */
	JButton rootbeerButton = new JButton(new ImageIcon(
			this.cldr.getResource("vending/rootbeer.jpg")));
	/**
	 * button to buy water
	 */
	JButton waterButton = new JButton(new ImageIcon(
			this.cldr.getResource("vending/water.jpg")));
	/**
	 * button to buy cola
	 */
	JButton colaButton = new JButton(new ImageIcon(
			this.cldr.getResource("vending/cola.jpg")));
	/**
	 * stock light for cola
	 */
	JLabel colaLight = new JLabel(this.colaStatus);
	/**
	 * stock light for lime
	 */
	JLabel limeLight = new JLabel(this.limeStatus);
	/**
	 * stock light for grape
	 */
	JLabel grapeLight = new JLabel(this.grapeStatus);
	/**
	 * stock light for root beer
	 */
	JLabel rootbeerLight = new JLabel(this.rootbeerStatus);
	/**
	 * stock light for water
	 */
	JLabel waterLight = new JLabel(this.waterStatus);
	/**
	 * beverage price display on machine
	 */
	JLabel price = new JLabel("Price: "
			+ this.currencyFormatter.format(this.cost));
	/**
	 * bebop cola art
	 */
	JLabel marquee = new JLabel(new ImageIcon(
			this.cldr.getResource("vending/bebop.jpg")));
	/**
	 * where you get your soda
	 */
	JLabel prizeHole = new JLabel();
	/**
	 * where the change is displayed
	 */
	JLabel changeHole = new JLabel();
	/**
	 * face of interface
	 */
	JPanel frontPanel = new JPanel(new GridLayout(1, 2));
	/**
	 * panel to hold all buttons
	 */
	JPanel controls = new JPanel(new GridLayout(0, 2));
	/**
	 * amount of change left after input money
	 */
	double change = 0;
	/**
	 * dispensed can decay timer object
	 */
	Timer canDecay = new Timer();
	

	/**
	 * applet init method
	 */
	@Override
	public void init() {
//here we set up the buttons
		this.input.addActionListener(this);
		this.input.setActionCommand("input");
		this.limeButton.addActionListener(this);
		this.limeButton.setActionCommand("lime");
		this.grapeButton.addActionListener(this);
		this.grapeButton.setActionCommand("grape");
		this.rootbeerButton.addActionListener(this);
		this.rootbeerButton.setActionCommand("rootbeer");
		this.waterButton.addActionListener(this);
		this.waterButton.setActionCommand("water");
		this.colaButton.addActionListener(this);
		this.colaButton.setActionCommand("cola");
//here we set up the interface
		add(this.frontPanel);
		this.frontPanel.add(this.marquee);
		this.frontPanel.add(this.controls);
		this.controls.add(this.price);
		this.controls.add(this.input);
		this.controls.add(this.colaButton);
		this.controls.add(this.colaLight);
		this.controls.add(this.limeButton);
		this.controls.add(this.limeLight);
		this.controls.add(this.grapeButton);
		this.controls.add(this.grapeLight);
		this.controls.add(this.rootbeerButton);
		this.controls.add(this.rootbeerLight);
		this.controls.add(this.waterButton);
		this.controls.add(this.waterLight);
		this.controls.add(this.prizeHole);
		this.controls.add(this.changeHole);

	}

	/**
	 * this method reacts to the buttons and checks moneyIn and causes soda dispensation and inventory depletion and changemaking
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		this.moneyIn = Double.parseDouble(this.input.getText());
		switch (e.getActionCommand()) {
		default:
			break;
		case "lime":
			if (this.limeVentory != 0 && moneyIn >= cost) {
				this.limeVentory--;
				this.change = this.moneyIn - this.cost;
				this.moneyIn = 1;
				refresh();
				showCan();
			}
			break;
		case "grape":
			if (this.grapeVentory != 0 && moneyIn >= cost) {
				this.grapeVentory--;
				this.change = this.moneyIn - this.cost;
				this.moneyIn = 1;
				refresh();
				showCan();
			}
			break;
		case "rootbeer":
			if (this.rootbeerVentory != 0 && moneyIn >= cost) {
				this.rootbeerVentory--;
				this.change = this.moneyIn - this.cost;
				this.moneyIn = 1;
				refresh();
				showCan();
			}
			break;
		case "water":
			if (this.waterVentory != 0 && moneyIn >= cost) {
				this.waterVentory--;
				this.change = this.moneyIn - this.cost;
				this.moneyIn = 1;
				refresh();
				showCan();
			}
			break;
		case "cola":
			if (this.colaVentory != 0 && moneyIn >= cost) {
				this.colaVentory--;
				this.change = this.moneyIn - this.cost;
				this.moneyIn = 1;
				refresh();
				showCan();
			}
			break;
		}
	}

	/**
	 * refresh display by checking stock and money totals
	 */
	private void refresh() {
		this.input.setText(String.valueOf(this.moneyIn));
		if (this.limeVentory == 0)
			this.limeStatus = this.out;
		if (this.colaVentory == 0)
			this.colaStatus = this.out;
		if (this.grapeVentory == 0)
			this.grapeStatus = this.out;
		if (this.rootbeerVentory == 0)
			this.rootbeerStatus = this.out;
		if (this.waterVentory == 0)
			this.waterStatus = this.out;
		this.colaLight.setIcon(this.colaStatus);
		this.limeLight.setIcon(this.limeStatus);
		this.grapeLight.setIcon(this.grapeStatus);
		this.rootbeerLight.setIcon(this.rootbeerStatus);
		this.waterLight.setIcon(this.waterStatus);
		this.changeHole.setText(this.currencyFormatter.format(this.change));
		

	}

	/**
	 * show a can in the output tray
	 */
	private void showCan() {
		this.canDecay.schedule(new decayTask(), 1000);
		this.prizeHole.setIcon(this.can);
	}
	
	/**
	 * @author Hunter
	 * timer task to decay the output soda
	 */
	class decayTask extends TimerTask {
		/** 
		 * blank the can output after a second
		 */
		@Override
		public void run() {
			SodaMachine.this.prizeHole.setIcon(new ImageIcon());
		}
	}

}
