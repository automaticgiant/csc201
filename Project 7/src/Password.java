/**
 * Project 7: Simple password strength test.
 * 
 * @author Hunter Morgan
 * @version 10/20/12
 */
class Password {
	// configure minimum digits and length here:
	/**
	 * minimum number of digits allowed by digitsTest
	 */
	private final static int	minDigits	= 2;

	/**
	 * minimum length of password allowed by lengthTest
	 */
	private final static int	minLength	= 8;

	/**
	 * this tests a string for a minimum number of digits
	 * 
	 * @param subject
	 *            what string we run this test on
	 * @return truth(pass/fail test)
	 */
	private static boolean digitsTest(String subject) {
		int digits = 0;
		for (char stringChar : subject.toCharArray()) {
			if (Character.isDigit(stringChar)) digits++;
		}
		if (digits >= minDigits) return true;
		return false;
	}

	/**
	 * this tests a string for a minimum length
	 * 
	 * @param subject
	 *            what string we run this test on
	 * @return truth(pass/fail test)
	 */
	private static boolean lengthTest(String subject) {
		if (subject.length() >= minLength) return true;
		return false;
	}

	/**
	 * this tests a string for a certain charset, in this case, numbers and
	 * letters
	 * 
	 * @param subject
	 *            what string we run this test on
	 * @return truth(pass/fail test)
	 */
	private static boolean charsTest(String subject) {
		for (char stringChar : subject.toCharArray()) {
			if (!Character.isLetterOrDigit(stringChar)) return false;
		}
		return true;
	}

	/**
	 * this either gets a string or strings from keyboard or arguments, tests
	 * it/them with the three test methods, and makes a ruling, in the case of
	 * arguments, also explains why invalid ones failed
	 * 
	 * @param args
	 *            strings to test
	 */
	public static void main(String[] args) {
		// if you didn't get any arguments, do what we were going to do
		if (args.length == 0) {
			// with only one use, explicit good.
			java.util.Scanner kb = new java.util.Scanner(System.in);
			String testString;
			System.out.print("Enter a string for password: ");
			testString = kb.nextLine();
			// combine the tests with boolean logic and use for an if/else
			if (digitsTest(testString) && lengthTest(testString)
					&& charsTest(testString)) System.out
					.println("valid password");
			else System.out.println("invalid password");
		}
		// if you did, test each one and explain why invalid ones fail
		else {
			for (String pwd : args) {
				if (digitsTest(pwd) && lengthTest(pwd) && charsTest(pwd)) System.out
						.println("\n" + pwd + " is a valid password.");
				else {
					System.out.println("\n" + pwd + " is an invalid password.");
					System.out.println("digitsTest:"
							+ Boolean.toString(digitsTest(pwd)));
					System.out.println("lengthTest:"
							+ Boolean.toString(lengthTest(pwd)));
					System.out.println("charsTest:"
							+ Boolean.toString(charsTest(pwd)));
				}
			}
		}
	}
}