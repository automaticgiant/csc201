import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

/**
 * 
 */

/**
 * @author 7student
 *
 */
class BeanCounter {

	/**
	 * No args, just procedure.
	 */
	public static void main(String[] args) {
		// No reason no to have everything in the main function.
		int num = 1;
		int max = 0;
		Scanner kbIn = new Scanner(System.in);
		HashMap<Integer, Integer> counts = new HashMap<Integer, Integer>();
		System.out.println("Give me numbers!");
		while ( num != 0 ) {
			num = kbIn.nextInt();
			if (counts.containsKey(num) == false) counts.put(num, 1);
			else {
				counts.put(num, counts.get(num) + 1);
			}
		}
		//iterate through value set once to establish max, second to remove less than. left with max set.
		Set<Integer> testingSet = counts.keySet();
		
		for ( int key : testingSet )
			if ( counts.get(key) > max ) max = counts.get(key);
		
		for ( Iterator<Integer> testingSetIterator = testingSet.iterator(); testingSetIterator.hasNext(); )
			if ( counts.get(testingSetIterator.next()) < max ) testingSetIterator.remove();
		
		System.out.print("Highest frequency is: " + max + ", belonging to");
		
		for ( int key : testingSet )
			System.out.print(" " + key);
		
		System.out.println(".");
		kbIn.close();
		
	}
}
