
/**
 * This class does the assigned things: reads in your first name, last name,
 * student Id, and class that you are taking this semester, and displays the
 * information one per line on the console and also on a dialog box.
 * 
 * @author Hunter Morgan 
 * @version 9/6/12
 */

import java.util.Scanner;
import javax.swing.JOptionPane;

public class MyInformation
{
    public static void main(String[] args)
       {
           String firstName; // Variable for first name
           String lastName; // Variable for last name
           String studentId; // Variable for studentid
           String classTaking; // Variable for class in
           String output; //Variable to store output for console and msgbox
           Scanner keyboard = new Scanner(System.in);
           System.out.println("What is your firstName?");
           firstName = keyboard.nextLine();
           System.out.println("What is your lastName?");
           lastName = keyboard.nextLine();
           System.out.println("What is your studentId?");
           studentId = keyboard.nextLine();
           System.out.println("What class are you taking?");
           classTaking = keyboard.nextLine();
           output = "Hello, " + firstName + " " + lastName + "!\nYour student ID is: " + studentId + "\nYour class " + classTaking + " is an excellent choice!\n";
           System.out.printf(output);
           JOptionPane.showMessageDialog(null, output);
        }
}
