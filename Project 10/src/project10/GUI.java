package project10;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * class definition for main gui object. handles actions.
 * 
 * @author Hunter Morgan
 */
class GUI extends JPanel implements ActionListener {

	/**
	 * this gets the action command passed by the buttons and fields and deals
	 * with it appropriately with a switch construct
	 * 
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "previous":
				if (this.trip.getTheDayIterator().hasPrevious()) this.currentDay = this.trip.getTheDayIterator().previous();
				refresh();
				break;
			case "remove":
				this.trip.getTheDayIterator().remove();
				if (this.trip.getTheDayIterator().hasPrevious()) this.currentDay = this.trip.getTheDayIterator().previous();
				else if (this.trip.getTheDayIterator().hasNext()) this.currentDay = this.trip.getTheDayIterator().next();
				else {
					this.trip.getTheDayIterator().add(new Day());
					this.trip.getTheDayIterator().previous();
				}
				refresh();
				break;
			case "new":
				this.trip.getTheDayIterator().add(new Day());
				this.currentDay = this.trip.getTheDayIterator().previous();
				refresh();
				break;
			case "next":
				if (this.trip.getTheDayIterator().hasNext()) this.currentDay = this.trip.getTheDayIterator().next();
				refresh();
				break;
			case "changeColor":
				// TODO change color code
				changeColor();
				break;
			// case "report":
			// // TODO report code
			// break;
			// case "changeBackground":
			// // TODO MMD background
			// changeBackground();
			// break;
			case "field":
				input();
				refresh();
				break;
		}
	}

	/**
	 * sometime you just need some debugging statements - this enables or
	 * disables some of them
	 */
	final boolean				debug			= false;
	// BuiltinInputHandler inputHandler = new BuiltinInputHandler();
	/**
	 * hashmap of all generated gui elements to facilitate later handling
	 */
	HashMap<String, JComponent>	componentHash	= new HashMap<>();
	/**
	 * trip we work with
	 */
	Trip						trip			= new Trip();
	/**
	 * represent day selected in gui - this is the cause of the bug where you
	 * have to next or previous twice to change direction
	 */
	Day							currentDay		= this.trip.getTheDayIterator().next();
	/**
	 * arraylist of GUIComponents, elements to generate
	 */
	ArrayList<GUIComponent>		GUIComponents;
	/**
	 * root gui object container
	 */
	JPanel						GUIRoot			= new JPanel(new BorderLayout());
	/**
	 * element containers
	 */
	@SuppressWarnings("javadoc")
	// this should generate good javadoc, but eclipse makes a fuss about
	// undocumented secondary single line declarations
	JPanel						dayData, tripData, dayControls, specialControls;

	// ImageIcon bgimage;

	/**
	 * this is the constructor for the gui (hmm, just realized i may have been
	 * able to avoid some stack overflow troubleshooting if i had made this a
	 * singleton)
	 */
	GUI() {

		super();
		setLayout(new BorderLayout());
		this.dayData = new JPanel(new GridLayout(0, 3));
		// dayData.setOpaque(false);
		this.tripData = new JPanel(new GridLayout(0, 3));
		// tripData.setOpaque(false);
		this.dayControls = new JPanel(new GridLayout(1, 0));
		// dayControls.setOpaque(false);
		this.specialControls = new JPanel(new GridLayout(0, 1));
		// specialControls.setOpaque(false);
		add(this.dayControls, BorderLayout.NORTH);
		add(this.dayData, BorderLayout.WEST);
		add(this.specialControls, BorderLayout.CENTER);
		add(this.tripData, BorderLayout.EAST);

		// GUIComponents.add(new GUIComponent("label", dayControls, "Day"));

	}

	/**
	 * this makes a string to display the day position in the trip
	 * 
	 * @return day label string
	 */
	String getCurrentDayLabel() {
		return "Day #" + (this.trip.getDays().indexOf(this.currentDay) + 1) + "/" + this.trip.getDays().size();
	}

	/**
	 * this populates the gui with elements generated from GUIComponents
	 */
	void populate() {
		//@formatter:off
		//gotcha:expense type order must match day.expenses order
		//no, wait, maybe not?
		this.GUIComponents = new ArrayList<>(Arrays.asList(
			new GUIComponent("label", this.dayControls, "Day"),
			new GUIComponent("button", this.dayControls, "<", "previous"),
			new GUIComponent("button", this.dayControls, "X", "remove"),
			new GUIComponent("button", this.dayControls, "new", "new"),
			new GUIComponent("button", this.dayControls, ">", "next"),
			
			new GUIComponent("label", this.dayData, "Daily"),
			new GUIComponent("label", this.dayData, "Cost"),
			new GUIComponent("label", this.dayData, "Reimbursed"),
			new GUIComponent("label", this.dayData, "Airfare"),
			new GUIComponent("field", this.dayData, "airfare"),
			new GUIComponent("label", this.dayData, "airfareReimbursement"),
			new GUIComponent("label", this.dayData, "Food"),
			new GUIComponent("field", this.dayData, "food"),
			new GUIComponent("label", this.dayData, "foodReimbursement"),
			new GUIComponent("label", this.dayData, "Car rental"),
			new GUIComponent("field", this.dayData, "carRental"),
			new GUIComponent("label", this.dayData, "carRentalReimbursement"),
			new GUIComponent("label", this.dayData, "Mileage"),
			new GUIComponent("field", this.dayData, "mileage"),
			new GUIComponent("label", this.dayData, "mileageReimbursement"),
			new GUIComponent("label", this.dayData, "Parking"),
			new GUIComponent("field", this.dayData, "parking"),
			new GUIComponent("label", this.dayData, "parkingReimbursement"),
			new GUIComponent("label", this.dayData, "Taxi"),
			new GUIComponent("field", this.dayData, "taxi"),
			new GUIComponent("label", this.dayData, "taxiReimbursement"),
			new GUIComponent("label", this.dayData, "Event"),
			new GUIComponent("field", this.dayData, "event"),
			new GUIComponent("label", this.dayData, "eventReimbursement"),
			new GUIComponent("label", this.dayData, "Lodging"),
			new GUIComponent("field", this.dayData, "lodging"),
			new GUIComponent("label", this.dayData, "lodgingReimbursement"),
			
			new GUIComponent("label", this.tripData, "Totals"),
			new GUIComponent("label", this.tripData, "Cost/miles"),
			new GUIComponent("label", this.tripData, "Reimbursed"),
			new GUIComponent("label", this.tripData, "Airfare"),
			new GUIComponent("label", this.tripData, "tripTotalAirfare"),
			new GUIComponent("label", this.tripData, "tripTotalAirfareReimbursement"),
			new GUIComponent("label", this.tripData, "Food"),
			new GUIComponent("label", this.tripData, "tripTotalFood"),
			new GUIComponent("label", this.tripData, "tripTotalFoodReimbursement"),
			new GUIComponent("label", this.tripData, "Car rental"),
			new GUIComponent("label", this.tripData, "tripTotalCarRental"),
			new GUIComponent("label", this.tripData, "tripTotalCarRentalReimbursement"),
			new GUIComponent("label", this.tripData, "Mileage"),
			new GUIComponent("label", this.tripData, "tripTotalMileage"),
			new GUIComponent("label", this.tripData, "tripTotalMileageReimbursement"),
			new GUIComponent("label", this.tripData, "Parking"),
			new GUIComponent("label", this.tripData, "tripTotalParking"),
			new GUIComponent("label", this.tripData, "tripTotalParkingReimbursement"),
			new GUIComponent("label", this.tripData, "Taxi"),
			new GUIComponent("label", this.tripData, "tripTotalTaxi"),
			new GUIComponent("label", this.tripData, "tripTotalTaxiReimbursement"),
			new GUIComponent("label", this.tripData, "Event"),
			new GUIComponent("label", this.tripData, "tripTotalEvent"),
			new GUIComponent("label", this.tripData, "tripTotalEventReimbursement"),
			new GUIComponent("label", this.tripData, "Lodging"),
			new GUIComponent("label", this.tripData, "tripTotalLodging"),
			new GUIComponent("label", this.tripData, "tripTotalLodgingReimbursement"),
			new GUIComponent("label", this.tripData, "Trip"),
			new GUIComponent("label", this.tripData, "tripTotalCosts"),
			new GUIComponent("label", this.tripData, "tripTotalReimbursement"),
			new GUIComponent("label", this.tripData, "Trip Net"),
			new GUIComponent("label", this.tripData, "tripNet"),
			new GUIComponent("label", this.tripData, ""),
			
			new GUIComponent("button", this.dayControls, "Color", "changeColor")
	//		new GUIComponent("button", specialControls, "Report", "report"),
	// new GUIComponent("button", specialControls, "Make My Day", "changeBackground")
		));
	//@formatter:on

		for (GUIComponent component : this.GUIComponents) {
			this.componentHash.put(component.label, component.build());
		}
	}

	/**
	 * this refreshes the outputs in the gui
	 */
	void refresh() {
		((JLabel) this.componentHash.get("Day")).setText(getCurrentDayLabel());

		// if (this.debug) System.out.println("currentDay " + this.currentDay +
		// ", expenses " + this.currentDay.expenses);
		// if (this.debug) for (Expense expense : this.currentDay.expenses)
		// System.out.println("ol expense " + expense);

		double[] expenseCostTotals = this.trip.getTripExpenseCostTotals();
		// if (true) System.out.println("array: " +
		// expenseCostTotals.toString());
		double[] expenseReimbursementTotals = this.trip.getTripExpenseReimbursementTotals();
		int i = 0;
		for (Expense expense : this.currentDay.expenses) {

			// refresh text fields/costs
			((JTextField) this.componentHash.get(expense.getType())).setText(String.valueOf(expense.getCost()));
			// refresh day labels/reimbursements
			((JLabel) this.componentHash.get(expense.getType() + "Reimbursement")).setText(String.valueOf(expense.getReimbursement()));

			// if (this.debug)
			// System.out.println("label " + "tripTotal" +
			// Character.toUpperCase(expense.getType().charAt(0)) +
			// expense.getType().substring(1) + ", = "
			// + String.valueOf(this.trip.getTotalCosts()));

			// refresh trip cost totals
			((JLabel) this.componentHash.get("tripTotal" + Character.toUpperCase(expense.getType().charAt(0)) + expense.getType().substring(1))).setText(String
					.valueOf(expenseCostTotals[i]));

			// if (false)
			// System.out.println("label " + "tripTotal" +
			// Character.toUpperCase(expense.getType().charAt(0)) +
			// expense.getType().substring(1)
			// + "Reimbursement");
			// if (true) System.out.println("i:" + i + ", ");

			// refresh trip reimbursement totals
			((JLabel) this.componentHash.get("tripTotal" + Character.toUpperCase(expense.getType().charAt(0)) + expense.getType().substring(1)
					+ "Reimbursement")).setText(String.valueOf(expenseReimbursementTotals[i]));
			i++;
		}
		// refresh trip totals
		((JLabel) this.componentHash.get("tripTotalCosts")).setText(String.valueOf(this.trip.getTotalCosts()));
		((JLabel) this.componentHash.get("tripTotalReimbursement")).setText(String.valueOf(this.trip.getTotalReimbursements()));
		((JLabel) this.componentHash.get("tripNet")).setText(String.valueOf(this.trip.getNetCost()));
	}

	/**
	 * this re-reads input from gui and sets things with mutators
	 */
	@SuppressWarnings("boxing")
	// i have warnings turned way up to make better code (ideally) but i didn't
	// feel like fixing the boxing suggestion for what's in the try
	void input() {
		for (Expense expense : this.currentDay.expenses) {

			// if (debug) System.out.print("il expense " + expense + ", " +
			// "type " + expense.getType());

			// JTextField field = (JTextField)
			// this.componentHash.get(expense.getType());

			// if (debug) System.out.print("field " + field + "\n");

			// input into expense cost mutators from text fields
			try {
				expense.setCost(Double.valueOf(((JTextField) this.componentHash.get(expense.getType())).getText()));
			} catch (NumberFormatException nfe) {
				expense.setCost(0.0);
			}
		}
	}

	/**
	 * change gui colors method
	 */
	void changeColor() {

		for (JComponent component : this.componentHash.values())
			component.setBackground(new Color(new Random().nextInt(16777215)));
		for (Component component : this.getComponents()) {
			// System.out.println(component);
			component.setBackground(new Color(new Random().nextInt(16777215)));
		}
	}

	// protected void paintComponent(Graphics g) {
	// super.paintComponent(g);
	// g.setColor(getBackground());
	// g.fillRect(0, 0, getWidth(), getHeight());
	//
	// System.out.println(bgimage);
	// System.out.println(g);
	// g.drawImage(bgimage, 0, 0, this);
	// }

	/**
	 * this was to change the background to a cat picture from the Internet. i
	 * spent hours trying to make it work, but alas, it was not meant to be.
	 */
	void changeBackground() {
		// i tried all kinds of background stuff but sadly couldn't make it work
		// String bgUrlString =
		// "http://cache-www.coderanch.com/images/bunkhouse_smoke.gif";
		// URL bgUrl = null;
		// try {
		//
		// bgUrl = new URL(bgUrlString);
		// System.out.println(bgUrl);
		// } catch (MalformedURLException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
		// bgimage = new ImageIcon(bgUrl);
		// System.out.println(bgimage);
		// dayControls.setOpaque(false);
		// tripData.setOpaque(false);
		// setIcon(bgimage);
		// repaint();
	}
}
