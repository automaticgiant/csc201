package project10;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * class definition for gui components to allow algorithmic element generation
 * 
 * @author Hunter Morgan
 */
class GUIComponent {
	/**
	 * parent container for element
	 */
	JPanel	container	= null;
	/**
	 * internal reference name for element
	 */
	String	label		= null;
	/**
	 * action for appropriate element types
	 */
	String	action		= null;
	/**
	 * type of element to create
	 */
	String	type		= null;

	// String expenseType = null;
	// boolean updates = false;
	// boolean reimbursement = false;

	/**
	 * Constructor for elements without actions
	 * 
	 * @param type
	 *            type of element to create
	 * @param panel
	 *            parent container for element
	 * @param label
	 *            internal reference name for element
	 */
	public GUIComponent(String type, JPanel panel, String label) {
		this.container = panel;
		this.label = label;
		this.type = type;
	}

	// /**
	// * @param type
	// * @param panel
	// * @param label
	// * @param updates
	// * @param reimbursement
	// * @param expenseType
	// */
	// public GUIComponent(String type, JPanel panel, String label, boolean
	// updates, boolean reimbursement, String expenseType) {
	// this.container = panel;
	// this.label = label;
	// this.type = type;
	// this.updates = updates;
	// this.reimbursement = reimbursement;
	// this.expenseType = expenseType;
	// }

	/**
	 * constructor for elements with actions
	 * 
	 * @param type
	 *            type of element to create
	 * @param panel
	 *            parent container for element
	 * @param label
	 *            internal reference name for element
	 * @param action
	 *            action for appropriate element types
	 */
	public GUIComponent(String type, JPanel panel, String label, String action) {
		this.container = panel;
		this.label = label;
		this.action = action;
		this.type = type;
	}

	/**
	 * gets called to build element and add to gui
	 * 
	 * @return element object
	 */
	public JComponent build() {
		JComponent component = null;
		switch (this.type) {
			case "button":
				component = new JButton(this.label);
				component.setName(this.label);
				this.container.add(component);
				((AbstractButton) component).setActionCommand(this.action);
				((AbstractButton) component).addActionListener(ExpenseGUIDriver.gui);
				break;
			case "label":
				component = new JLabel(this.label);
				component.setName(this.label);
				this.container.add(component);
				break;
			case "field":
				component = new JTextField();
				component.setName(this.label);
				this.container.add(component);
				((JTextField) component).setActionCommand("field");
				((JTextField) component).addActionListener(ExpenseGUIDriver.gui);
				break;
		}
		return component;
	}

}