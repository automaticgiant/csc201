package project10;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

import project10.Expense.AirfareExpense;
import project10.Expense.CarRentalExpense;
import project10.Expense.EventExpense;
import project10.Expense.FoodExpense;
import project10.Expense.LodgingExpense;
import project10.Expense.MileageExpense;
import project10.Expense.ParkingExpense;
import project10.Expense.TaxiExpense;

/**
 * day class - component of a trip object
 * 
 * @author Hunter Morgan
 */
public class Day {

	/**
	 * arraylist of types of expenses for the day
	 */
	//@formatter:off
	public ArrayList<Expense>	expenses	= new ArrayList<>(Arrays.asList(
			new AirfareExpense(), new ParkingExpense(), new MileageExpense(),
			new EventExpense(), new LodgingExpense(), new CarRentalExpense(),
			new TaxiExpense(), new FoodExpense()));
	//@formatter:on

	/**
	 * expense arraylist iterator accessor/generator
	 * 
	 * @return iterator for expense arraylist
	 */
	public ListIterator<Expense> getExpenseIterator() {
		return this.expenses.listIterator();
	}

	/**
	 * @return total expenses incurred this day
	 */
	public double getTotalCosts() {
		double totalCost = 0;
		for (Expense expense : this.expenses) {
			totalCost += expense.getCost();
		}
		return totalCost;
	}

	/**
	 * @return total reimbursed for this day
	 */
	public double getTotalReimbursements() {
		double totalReimbursements = 0;
		for (Expense expense : this.expenses) {
			totalReimbursements += expense.getReimbursement();
		}
		return totalReimbursements;
	}

	/**
	 * @return net expenses for this day
	 */
	public double getNetCost() {
		return getTotalCosts() - getTotalReimbursements();
	}
}
