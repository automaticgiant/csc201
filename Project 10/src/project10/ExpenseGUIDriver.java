/**
 * 
 */
package project10;

import javax.swing.JFrame;

/**
 * the driver class to run the app
 * 
 * @author Hunter Morgan
 */
public class ExpenseGUIDriver {
	/**
	 * gui object that will generate and display the interface
	 */
	static GUI		gui;
	/**
	 * root window where the gui panel will go
	 */
	static JFrame	Window	= new JFrame();

	/**
	 * @param args
	 *            moot convention
	 */
	public static void main(String[] args) {
		gui = new GUI();
		// gui.setOpaque(false);
		// gui.setBackground(new Color(0, 0, 0, 0));
		gui.populate();
		gui.refresh();
		final int WINDOW_WIDTH = 700, WINDOW_HEIGHT = 400;
		Window.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		Window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Window.setLocationRelativeTo(null);
		Window.add(gui);
		Window.setVisible(true);

	}
}
