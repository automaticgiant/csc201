package project10;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * class for a trip composed of days
 * 
 * @author Hunter Morgan
 */
public class Trip {

	/**
	 * accessor for to calculate expense type cost totals
	 * 
	 * @return expense type cost totals array
	 */
	public double[] getTripExpenseCostTotals() {
		double[] expenseTotals = new double[new Day().expenses.size()];
		for (Day day : getDays()) {
			int i = 0;
			for (Expense expense : day.expenses) {
				expenseTotals[i] += expense.getCost();
				// if (true) System.out.println("day:" + day + ", expense:" +
				// expense + ", total:" + expenseTotals[i]);
				i++;
			}
		}
		return expenseTotals;
	}

	/**
	 * accessor for to calculate expense type reimbursement totals
	 * 
	 * @return expense type totals reimbursement array
	 */
	public double[] getTripExpenseReimbursementTotals() {
		double[] expenseTotals = new double[new Day().expenses.size()];
		for (Day day : this.days) {
			// if (false) System.out.println(day);
			int i = 0;
			for (Expense expense : day.expenses) {
				expenseTotals[i++] += expense.getReimbursement();
			}
		}
		return expenseTotals;
	}

	/**
	 * basic trip constructor
	 */
	public Trip() {
		this.days.add(new Day());
	}

	/**
	 * days which make up trip
	 */
	private ArrayList<Day>		days	= new ArrayList<>();

	/**
	 * iterator for days
	 */
	private ListIterator<Day>	dayIterator;

	/**
	 * days arraylist accessor
	 * 
	 * @return # of days in trip
	 */
	public ArrayList<Day> getDays() {
		return this.days;
	}

	/**
	 * get the or a iterator for the trip. singleton
	 * 
	 * @return the ListIterator
	 */
	public ListIterator<Day> getTheDayIterator() {
		if (this.dayIterator == null) this.dayIterator = this.days.listIterator();
		return this.dayIterator;
	}

	/**
	 * Accessor for days iterator
	 * 
	 * @return listIterator for days
	 */
	public ListIterator<Day> getDayIterator() {
		return this.days.listIterator();
	}

	/**
	 * accessor for total trip cost
	 * 
	 * @return total expenses incurred this trip
	 */
	public double getTotalCosts() {
		double totalCost = 0;
		for (Day day : this.days) {
			totalCost += day.getTotalCosts();
		}
		return totalCost;
	}

	/**
	 * accessor for total trip reimbursements
	 * 
	 * @return total reimbursed for this trip
	 */
	public double getTotalReimbursements() {
		double totalReimbursements = 0;
		for (Day day : this.days) {
			totalReimbursements += day.getTotalReimbursements();
		}
		return totalReimbursements;
	}

	/**
	 * accessor for net cost for the trip
	 * 
	 * @return net cost for this trip
	 */
	public double getNetCost() {
		return getTotalCosts() - getTotalReimbursements();
	}

}
