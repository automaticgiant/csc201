
import java.util.Scanner;

/**
 *  This class does the assigned things: It prompts for input
 *  of taxable income and outputs computed tax.
 *  @author Hunter Morgan 
 *  @version 9/27/12
 */
class TaxComputer {

/**
 * This is the routine where all the magic happens. No arguments.
 */
	public static void main(String[] args) {
		// variables
        int taxable;
        float tax;
        Scanner keyboard = new Scanner(System.in); // input object
        
        System.out.print("Please enter your taxable income:   ");
        taxable = keyboard.nextInt();
        //if ( taxable < 0 ) i was going to some error handling/input validation;
        //					 i like that as much as the next guy, but it doesn't
        //					 seem to be required by this assignment
        if ( taxable < 10000 ) 
        	tax = (float) (.1 * taxable);
        else if ( taxable < 30000 )
        	tax = (float) (.15 * taxable);
        else if ( taxable < 60000 )
        	tax = (float) (.25 * taxable);
        else if ( taxable < 130000 )
        	tax = (float) (.28 * taxable);
        else if ( taxable < 250000 )
        	tax = (float) (.33 * taxable);
        else
        	tax = (float) (.35 * taxable);
        System.out.format("Tax is $%.2f", tax);
	}
}